function filter(elements, callBack) {
    if (elements == undefined || callBack == undefined) {
        return [];
    } else {
        let evenNumbers = [];
        for (let index = 0; index < elements.length; index++) {
            if (callBack(elements[index], index, elements)==true) {
                evenNumbers.push(elements[index]);
            }
        }
        return evenNumbers;
    }
    return [];
}

module.exports = filter;