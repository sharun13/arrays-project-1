function each(elements, callBack) {
    if (elements == undefined || callBack == undefined) {
        return [];
    } else {
        let resultArray = [];
        for (let index = 0; index < elements.length ; index++) {
            resultArray.push(callBack(elements[index], index));
        }
        return resultArray;
    }
    return [];
}

module.exports = each;
