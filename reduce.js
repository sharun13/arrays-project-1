function reduce(elements, callBack, startingValue) {
    if (elements == undefined) {
        return [];
    }
    let result;
    if (startingValue == undefined) {
        startingValue = elements[0];
        result = startingValue;
        for (let index = 1; index < elements.length; index++) {
            result = callBack(result, elements[index], index, elements)
        }
    } else {
        result = startingValue;
        for (let index = 0; index < elements.length; index++) {
            result = callBack(result, elements[index], index, elements)
        }
    }

    return result;
    
}

module.exports = reduce;