function flatten(elements, depth) {
    if (depth == undefined) {
        depth = 1;
    }
    let flatArray = [];
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index]) && depth > 0) {
            flatArray = flatArray.concat(flatten(elements[index], depth - 1));
        } else if (elements[index] == undefined) {
            continue;
        } else {
            flatArray.push(elements[index]);
        }
    }
    return flatArray;
}

module.exports = flatten;