let array = require('../array1');
let each = require('../each');

function callBack(element, index) {
    return (element * 2);
}

const result = each(array, callBack);
console.log(result);