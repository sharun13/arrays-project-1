function find(elements, callBack) {
    if (elements == undefined || callBack == undefined) {
        return [];
    } else {
        for (let index = 0; index < elements.length; index++) {
            if (callBack(elements[index])) {
                return elements[index];
                break;
            }
        }
    }
    return undefined;
}

module.exports = find;